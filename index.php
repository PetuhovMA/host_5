<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $messages = array();

    if (!empty($_COOKIE['save'])) {
        
        setcookie('save', '', 100000);
        $messages[] = 'Матрица была обновлена';
        if (!empty($_COOKIE['pass'])) {
          $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
            и паролем <strong>%s</strong> для изменения данных матрицы.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
        }
    }
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['yob'] = !empty($_COOKIE['yob_error']);
    $errors['_value'] = !empty($_COOKIE['_value_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['sp-sp'] = !empty($_COOKIE['sp-sp_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['galka'] = !empty($_COOKIE['galka_error']);
    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div class="fioerror">  Заполните имя.</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="emailerror">  Заполните email.</div>';
    }
    if ($errors['yob']) {
        setcookie('yob_error', '', 100000);
        $messages[] = '<div class="yoberror">  Укажите дату.</div>';
    }
    if ($errors['_value']) {
        setcookie('_value_error', '', 100000);
        $messages[] = '<div class="_valueerror">  Если вы безногий, жмите любой вариант</div>';
    }
    if ($errors['gender']) {
        setcookie('gender_error', '', 100000);
        $messages[] = '<div class="gendererror">  Укажите пол.</div>';
    }
    if ($errors['sp-sp']) {
        setcookie('sp-sp_error', '', 100000);
        $messages[] = '<div class="sp-sperror">  Если вы не любите музыку, выберите последний вариант</div>';
    }
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="bioerror">  Заполните биографию.</div>';
    }
    if ($errors['galka']) {
        $messages[] = '<div class="galkaerror">  Поставьте галочку.</div>';
    }
    if (!$errors['fio'] && !$errors['email'] && !$errors['yob'] && !$errors['_value'] && !$errors['gender'] && !$errors['sp-sp'] && !$errors['bio'] && !$errors['galka']  ) {
        if (!empty($_COOKIE['save'])) {
            setcookie('save', '', 100000);
            $messages[] = 'Ваши данные надёжно спрятаны под камнем';
          }
    }
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['yob'] = empty($_COOKIE['yob_value']) ? '' : $_COOKIE['yob_value'];
    $values['_value'] = empty($_COOKIE['_value_value']) ? '' : $_COOKIE['_value_value'];
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['sp-sp'] = empty($_COOKIE['sp-sp_value']) ? '' : $_COOKIE['sp-sp_value'];
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['galka'] = empty($_COOKIE['galka_value']) ? '' : $_COOKIE['galka_value'];
    if (isset($_SESSION['login'])) {
        $login = $_SESSION['login'];
        $conn = new PDO("mysql:host=localhost;dbname=u20989", 'u20989', '5299306', array(PDO::ATTR_PERSISTENT => true));
        $user = $conn->prepare("UPDATE Гsers SET fio = ?, email = ?, yob = ?, gender = ?, _value = ?, bio = ? WHERE `login`='$login'");
        if(isset($_COOKIE['fio_value'])){
            $user -> execute([$_COOKIE['fio_value'], $_COOKIE['email_value'], $_COOKIE['yob_value'], $_COOKIE['gender_value'], $_COOKIE['_value_value'], $_COOKIE['bio_value']]);
            setcookie('save', '', 100000);
        }
        $messages[] = '<br>Очередное ваше появление вызвало сбой в матрице' .$_SESSION['login'] ;
    }
    include('form.php');
    exit();
}
else{
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['yob'])) {
        setcookie('yob_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('yob_value', $_POST['yob'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['_value'])) {
        setcookie('_value_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('_value_value', $_POST['_value'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['gender'])) {
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['sp-sp'])) {
        setcookie('sp-sp_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('sp-sp_value', $_POST['sp-sp'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['galka'])) {
        setcookie('galka_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('galka_value', $_POST['galka'], time() + 30 * 24 * 60 * 60);
    }
    if ($errors) {
        header('Location: index.php');
        exit();
      }
      else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('yob_error', '', 100000);
        setcookie('_value_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('sp-sp_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('galka_error', '', 100000);
    }

    if (!empty($_SESSION['login'])){
    $conn = new PDO("mysql:host=localhost;dbname=u20989", 'u20989', '5299306', array(PDO::ATTR_PERSISTENT => true));
    $user = $conn->prepare("UPDATE Users SET fio = ?, email = ?, yob = ?, gender = ?, _value = ?, bio = ?, login = ?, pass = ? WHERE `login`='$login' AND `pass` = 
        '$pass'");
        if(isset($_POST['fio'])){
            $user -> execute([$_POST['fio'], $_POST['email'], $_POST['yob'], $_POST['gender'], $_POST['_value'], $_POST['bio'], $login, $passmd5 ]);
            setcookie('save', '', 100000);
        }
        echo "часть кода 3";
        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    }
    else {
        //создаем логин и пароль для пользователя
        $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $maxp=10;
        $maxl=5;
        $size=StrLen($chars)-1;
        $pass=null;
        while($maxp--){
            $pass.=$chars[rand(0,$size)];
        }
        while($maxl--){
            $login.=$chars[rand(0,$size)];
        }
        $passmd5 = md5($pass);
        setcookie('login', $login);
        setcookie('pass', $pass);
        ///////////////
        //вставляем нового пользователя
        $conn = new PDO("mysql:host=localhost;dbname=u20989", 'u20989', '5299306', array(PDO::ATTR_PERSISTENT => true));
        $user = $conn->prepare("INSERT INTO Users SET fio = ?, email = ?, yob = ?, gender = ?, _value = ?, bio = ?, login = ?, pass = ?");
        if(isset($_POST['fio'])){
            $user -> execute([$_POST['fio'], $_POST['email'], $_POST['yob'], $_POST['gender'], $_POST['_value'], $_POST['bio'], $login, $passmd5 ]);
            setcookie('save', '', 100000);
            
        }

    $foot = $conn->prepare("INSERT INTO Foots SET id_user = ?");
    $foot -> execute([$id_user]);
    $id_music = $conn->lastInsertId();
    if(isset($_POST['sp-sp'])){
        $music = $conn->prepare("INSERT INTO Music SET musicID = ?, music_names = ?");
     $music -> execute([$id_music, $music]);
    }
}
    setcookie('save', '1');
    header("Location: ?save=1");
}
?>
