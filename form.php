<!DOCTYPE html>
<head>
  <link rel="stylesheet" href="style.css">
  <meta charset="utf-8">
  <title>Мать природа</title>
</head>
<body >
  <header>
  <div class="banner">
  <a href="#home"><img id="logo" src="images/totem.jpg" width="100" height="100"  alt="Логотип"/></a>
 
  <h1>Природа зовёт</h1>
</div>
</header>

<div class="container">
  <div class="forma">
  <h2 id="form">Ваша форма</h2>
  <form method="POST">
    <ol>
    <?php
        if (!empty($messages)) {
        print('<div id="messages">');
        foreach ($messages as $message) {
          print($message);
        }
        print('</div>');
        }
      ?>
      <li>ФИО: <input name="fio" placeholder="Введите ФИО" 
      <?php if ($errors['fio']) {print 'class="fioerror"';} ?>
       value="<?php print $values['fio']; ?>" /> 
      </li>
      <li>E-mail: <input name="email" placeholder="Введите email" type="email" 
      <?php if ($errors['email']) {print 'class="emailerror"';} ?> value="<?php print $values['email']; ?>" /></li>
      <li>Дата рождения: <input name="yob" type="date"
      <?php if ($errors['yob']) {print 'class="yoberror"';} ?> value="<?php print $values['yob']; ?>" /> </li>
      <li>
        Пол :
        <input type="radio" checked="checked" name="gender" value="man"<?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Муж.
        <input type="radio" name="gender" value="woman"<?php if ($errors['gender']) {print 'class="gendererror"';} ?>/>Жен.
        <input type="radio" name="gender" value="zver"<?php if ($errors['gender']) {print 'class="gendererror"';} ?>/> Зверь
      </li>
      <li>
        Ваш размер обуви:
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="1"/>39
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="2"/>40
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="3"/>41
        <input type="radio" checked="checked" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?> value="4"/>42
        <input type="radio" name="_value" <?php if ($errors['_value']) {print 'class="_valueerror"';} ?>value="5"/>50
      </li>
      <li>
        Любимая музыка:
        <select name="sp-sp" multiple="multiple" <?php if ($errors['sp-sp']) {print 'class="musicerror"';} ?>>
          <option value="rok">Рок</option>
          <option value="metall">Металл</option>
          <option value="pop">Поп</option>
          <option value="wonder_sounds">Звуки природы</option>
        </select>
      </li>
      <li>
        Биография :<textarea name="bio" 
        <?php if ($errors['bio']) {print 'class="bioerror"';} ?> 
        placeholder="Введите чью-либо биографию" ></textarea>
      </li>
      <li>
        <input type="checkbox" name="galka" <?php if ($errors['galka']) {print 'class="galkaerror"';} ?> />С условиями ознакомлена(ы)
      </li>
      <li>
        <input type="submit" value="Отправить" />
      </li>
    </ol>
    <?php
      if(!isset($_COOKIE['login'])){
        ?>
        <a href="login.php">Я уже являюсь частью матрицы</a>
        <?php
      }
      else{
        ?>
        <a href="antiLogin.php">Я хочу стать частью матрицы</a>
        <?php
      }
    ?>
  </form>
</div>
</div>
<footer>
<h2>Если природа зовёт, лучше не терпеть</h2>
</footer>
</body>
</html>
